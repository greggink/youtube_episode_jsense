#include <stdio.h>
#include "jsense.h"

void main(){

	JSENSE *j = jse_from_file("json_samples/testing.json");

	printf("Name: %s\n", jse_get(j, "name"));
	printf("Age: %s\n", jse_get(j, "age"));

	char *friend = jse_get(j, "friends[0]");
	int i = 1;
	while(friend){
		printf("%d. %s\n", i, friend);
		friend = jse_get_next(j);
		i += 1;
	}

}//main*/
