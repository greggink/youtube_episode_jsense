#include <stdio.h>
#include "jsense.h"

void main(){

	JSENSE *j = jse_from_file("json_samples/github_example.json");

	char *text = jse_get(j, "[0].commit.author.name");
	printf("name is %s\n\n", text);
	jse_free(j);

	j = jse_from_file("/usr/lib/linuxmint/mintSources/countries.json");
	text = jse_get(j, "[1].name.native.pus.official");
	printf("text is %s\n", text);

	text = jse_get(j, "[1].name.native.pus.common");
	printf("text (common) is %s\n", text);

	int error;
	text = jse_get(j, "[2].translations.spa.official");
	printf("text is %s\n", text);
	text = jse_get(j, "[2].latlng.[1]");
	printf("longitude is %s double is %g\n", text, tec_string_to_double(text, &error));
	jse_free(j);

}//main*/
